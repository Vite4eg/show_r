<?php

/**
 * Выводит информацию о переменной на экран
 *
 * @param  mixed    $expression  Выражение для вывода на экран
 * @param  boolean  $printType   Необходимо ли выводить тип переменной. По умолчанию true
 * @param  bool     $return      Возвращать результат в переменную
 *
 * @return mixed Если $return = true, вернет результат как готовую html страницу (со всяким html, head, body)
 */
function show_r($expression, $printType = true, $return = false)
{
    /**
     * Массив соответствий типа переменной и цвета, которым эту переменную выводить
     * Ключи должны быть в нижнем регистре
     */
    $arColor = array(
        'integer' => '#4e9a06',
        'string'  => '#cc0000',
        'boolean' => '#75507b',
        'null'    => '#3465a4'
    );

    /**
     * Возврат цвета
     *
     * Оборачивает значение в '<span style="color: #hex"></span>'
     * Для соответствий используется массив $arColor
     *
     * @param  mixed    $value  Значение
     *
     * @return string   Строка вида <span style="color: #hex"></span>
     */
    $getColor = function($value) use ($arColor)
    {
        $type = strtolower(gettype($value));

            // для разных видов переменных надо текст предусмотреть
        if (is_null($value) || is_object($value) || is_array($value)) {
            $value = $type;
        } else if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        } else if (is_string($value)) {
            $value = "'{$value}'";
        }

            // покрасим вывод
        if (isset($arColor[$type]) && !empty($arColor[$type])) {
            $result = "<span style=\"color: {$arColor[$type]};\">{$value}</span>";
        } else {
            $result = $value;
        }

        return $result;
    };

    /**
     * Возвращает описание типа значения
     *
     * @param   mixed   $value
     *
     * @return  string
     */
    $getType = function ($value) use ($printType)
    {
        if (!$printType) {
            return '';
        }

        if (is_object($value)) {
            $result = get_class($value);
        } else if (is_array($value)) {
            $result = count($value);
        } else {
            $result = gettype($value);
        }

        return "<small>({$result})</small>";
    };

    /**
     * Вывод обычных данных (не объекта, не массива)
     */
    $printData = function ($data = array('key' => '', 'value' => '', 'modificator' => '')) use (&$getType, &$getColor)
    {
        ?>
            <?if (!empty($data['modificator'])) :?>
                <i><?= $data['modificator'];?></i>
            <?endif;?>
            <?if (!empty($data['key'])) :?>
                '<?= $data['key'];?>' &#8658;
            <?endif;?>
            <?= $getColor($data['value']);?>
            <?= $getType($data['value']);?>
        <?
    };

    /**
     * Получить описание свойств объекта
     *
     * @param object $object Объект какого-то класса
     */
    $printObject = function ($object) use (&$getColor, &$getType, &$printObject, &$printArray, &$printData)
    {
            // используем ReflectionAPI
        $rflObject = new ReflectionObject($object);
        $obProperties = $rflObject->getProperties();    // список свойств класса
        ?>
        <ul>
        <?
        foreach ($obProperties as $rflProperty) {
            $modifier = implode(' ', Reflection::getModifierNames($rflProperty->getModifiers()));

            // прежде чем получать значения свойства protected или private, его надо сделать доступным
            if ($rflProperty->isPrivate() || $rflProperty->isProtected()) {
                $rflProperty->setAccessible(true);
            }
            $value = $rflProperty->getValue($object);
            ?>
                <?$id = uniqid('id'); // уникальный id для связывания input и label ?>
                <li title="<?=gettype($value)?>">

                    <? if (is_array($value) && !empty($value)) :?>
                        <input type="checkbox" id="<?= $id;?>" >
                        <label for="<?= $id;?>">
                            <?$printData(array('key' => $rflProperty->getName(), 'value' => $value, 'modificator' => $modifier));?>
                        </label>
                        <?$printArray($value);?>
                    <? elseif (is_object($value)) :?>
                        <input type="checkbox" id="<?= $id;?>" >
                        <label for="<?= $id;?>">
                            '<?=$key?>' &#8658; object <small>(<?= get_class($value);?>)</small>
                        </label>
                        <?$printObject($value);?>
                    <? else :?>
                        <?$printData(array('key' => $rflProperty->getName(), 'value' => $value, 'modificator' => $modifier));?>
                    <? endif; ?>
                </li>
            <?
        }
        ?>
        </ul>
        <?
        unset($obProperties);
        unset($rflObject);
    };

    /**
     * Рекурсивная функция для шагания по вложенным массивам
     *
     * @param $array
     */
    $printArray = function($array) use (&$printArray, &$getColor, &$getType, &$printObject, &$printData)
    {
        ?>
        <ul>
            <? foreach ($array as $key => $value) :?>
                <?$id = uniqid('id'); // уникальный id для связывания input и label ?>
                <li title="<?= gettype($value);?>">
                    <? if (is_array($value) && !empty($value)) :?>
                        <input type="checkbox" id="<?= $id;?>" />
                        <label for="<?= $id;?>">
                            '<?=$key?>' &#8658; array <small>(<?= count($value);?>)</small>
                        </label>
                        <?$printArray($value);?>
                    <? elseif (is_object($value)) :?>
                        <input type="checkbox" id="<?= $id;?>" />
                        <label for="<?= $id;?>">
                            '<?=$key?>' &#8658; object <small>(<?= get_class($value);?>)</small>
                        </label>
                        <?$printObject($value);?>
                    <? else :?>
                        <?$printData(array('key' => $key, 'value' => $value));?>
                    <? endif; ?>
                </li>
            <? endforeach; ?>
        </ul>
        <?
    };

    $cl = uniqid('show');    // класс для <div>, в котором будет все размещаться
    ?>

    <?ob_start();?>
    <div class="<?= $cl;?>">
        <style type="text/css">
            .<?=$cl;?> {font-size:14px; line-height: 1.5; margin: 1em 0 1em 1em; padding: 0.7em 0 0.75em 1.5em; font-family: monospace; background-color: #f7f7f7; border-radius: 3px;}
            .<?=$cl;?> div,
            .<?=$cl;?> ul,
            .<?=$cl;?> pre,
            .<?=$cl;?> li {line-height: 1}
            .<?=$cl;?> h3 {margin : 0 0 0.3em -0.7em;}
            .<?=$cl;?> label {cursor: pointer;}
            .<?=$cl;?> ul {list-style-type: none; padding: 0; margin: 0;}
            .<?=$cl;?> li {position: relative; padding: 0; margin: 0;}
            .<?=$cl;?> li ul {display: none; margin-left: 1.5em;}
            .<?=$cl;?> input[type=checkbox] {display: none;}
            .<?=$cl;?> input + label:before {content : '+'; position: absolute; left: -0.75em;}
            .<?=$cl;?> input:checked ~ ul {display: block;}
            .<?=$cl;?> input:checked + label:before {content : "\2013";}
        </style>

        <? if (is_array($expression)) :?>
            <h3>array <?= $getType($expression);?></h3>
            <? $printArray($expression); ?>
        <? elseif (is_object($expression)) :?>
            <h3>array <?= $getType($expression);?></h3>
            <? $printObject($expression); ?>
        <? else :?>
            <?= $printData(array('value' => $expression));?>
        <? endif; ?>
    </div>
    <?
    $html = ob_get_contents();
    ob_get_clean();

    if ($return) {
        $result = '<html><head><meta charset="UTF-8"/></head><body>' . $html . '</body></html>';
        return $result;
    } else {
        echo $html;
    }
};
